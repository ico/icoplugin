package fr.ico.plugin;

import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class ViewHandler {
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell s) throws PartInitException {
		
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView("fr.ico.plugin.partDescFragment.ASampleE4View");
		

	}
}
