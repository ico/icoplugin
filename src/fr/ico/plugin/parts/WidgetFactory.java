package fr.ico.plugin.parts;

import java.text.Collator;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import fr.ico.lib.optimization.storage.Suggestion;

public class WidgetFactory {

	public static Composite composite(Composite parent, Layout layout) {
		return composite(parent, layout, SWT.BORDER);
	}

	public static void createSuggestionLabels(Suggestion suggestion, Composite parent, ICOPluginView view) {
		DecimalFormat df = new DecimalFormat("#.##");
        String gain = df.format(suggestion.getPercentage());
		label(parent, "Remove: " + suggestion.getToRemoveName(), new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1)).setForeground(Display.getDefault().getSystemColor(SWT.COLOR_RED));
		label(parent, "Add:    " + suggestion.getToAddName(), new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1)).setForeground(Display.getDefault().getSystemColor(SWT.COLOR_GREEN));
		label(parent, "+" + gain + "%");
		button(parent, null, "Apply", e -> view.apply(suggestion));
	}

	public static Composite composite(Composite parent, Layout layout, int style) {
		Composite composite = new Composite(parent, style);
		composite.setLayout(layout);
		return composite;
	}
	
	public static RowLayout rowLayout(int type) {
		RowLayout layout = new RowLayout(type);
		layout.wrap = false;
		layout.pack = false;
		layout.justify = true;
		return layout;
	}

	public static ScrolledComposite scrolledComposite(Composite parent, Control content) {
		ScrolledComposite sComposite = new ScrolledComposite(parent, SWT.BORDER | SWT.V_SCROLL);
		content.setParent(sComposite);
		sComposite.setContent(content);
		sComposite.setExpandVertical(true);
		sComposite.setExpandHorizontal(true);
		sComposite.setMinSize(content.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		return sComposite;
	}

	public static TabItem tabItem(TabFolder parent, String text, Control control) {
		TabItem tabItem = new TabItem(parent, SWT.BORDER);
		tabItem.setText(text);
		tabItem.setControl(control);
		return tabItem;
	}

	public static List list(Composite parent) {
		List list = new org.eclipse.swt.widgets.List(parent,
				SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		list.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		return list;

	}

	public static Button button(Composite parent, GridData data, String text, Consumer<SelectionEvent> c) {
		Button button = new Button(parent, SWT.PUSH | SWT.CENTER);
		button.setText(text);
		if (data != null)
			button.setLayoutData(data);
		button.addSelectionListener(widgetSelectedAdapter(c));
		return button;
	}

	public static Label label(Composite parent, String text) {
		return label(parent, text, null);
	}

	public static Label label(Composite parent, String text, Object data) {
		Label label = new Label(parent, SWT.NONE);
		label.setText(text);
		if (data != null)
			label.setLayoutData(data);
		return label;
	}

	public static TableItem tableItem(Table table, String key, int value) {
		TableItem item = new TableItem(table, SWT.NONE);
		item.setText(new String[] { key, Integer.toString(value) });
		return item;
	}

	public static Table table(Composite parent, String col1, String col2, GridData data) {
		final Table table = new Table(parent, SWT.BORDER);
		table.setHeaderVisible(true);

		TableColumn column1 = column(table, col1, 250);
		TableColumn column2 = column(table, col2, 100);

		Listener sortListener = new Listener() {
			public void handleEvent(Event e) {
				TableItem[] items = table.getItems();
				Collator collator = Collator.getInstance(Locale.getDefault());
				TableColumn column = (TableColumn) e.widget;
				int index = column == column1 ? 0 : 1;
				for (int i = 1; i < items.length; i++) {
					String value1 = items[i].getText(index);
					for (int j = 0; j < i; j++) {
						String value2 = items[j].getText(index);
						if ((table.getSortDirection() == SWT.UP &&  collator.compare(value1, value2) < 0) || (table.getSortDirection() == SWT.DOWN &&  collator.compare(value1, value2) > 0)) {
							String[] values = { items[i].getText(0), items[i].getText(1) };
							items[i].dispose();
							TableItem item = new TableItem(table, SWT.NONE, j);
							item.setText(values);
							items = table.getItems();
							break;
						}
					}
				}
				table.setSortColumn(column);
				table.setSortDirection(SWT.UP == table.getSortDirection() ? SWT.DOWN : SWT.UP);
			}
		};

		column1.addListener(SWT.Selection, sortListener);
		column2.addListener(SWT.Selection, sortListener);
		table.setSortColumn(column2);
		table.setSortDirection(SWT.DOWN);
		if (data != null) table.setLayoutData(data);
 
		return table;
	}

	public static TableColumn column(Table parent, String text, int width) {
		final TableColumn column = new TableColumn(parent, SWT.NONE);
		column.setText(text);
		column.setWidth(width);
		return column;
	}

	public static SelectionListener widgetSelectedAdapter(Consumer<SelectionEvent> c) {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				c.accept(e);
			}
		};
	}

}
