package fr.ico.plugin.parts;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Consumer;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

import fr.ico.lib.optimization.ConfigImprovement;
import fr.ico.lib.optimization.Optimization;
import fr.ico.lib.optimization.storage.ConfigPerformanceItem;
import fr.ico.lib.optimization.storage.Suggestion;
import fr.ico.lib.optimization.utils.FileHandling;
import fr.ico.lib.optimization.utils.Logger;
import fr.ico.lib.optimization.utils.Logger.LogObserver;

public class ICOPluginView implements LogObserver {
	
	private Label logLabel;
	private Composite featureComposite;
	private Composite interactionComposite;
	private Composite constrainteComposite;
	private Composite detailsComposite;
	
	private ConfigImprovement configImprovement;
	
	private List whiteList;
	private List blackList;
	private List freeList;
	

	@PostConstruct
	public void createPartControl(Composite parent) throws CoreException, IOException {
		Logger.subscribe(this);
		
		TabFolder tabFolder = new TabFolder(parent, SWT.BORDER);
		
		// Tab content
		featureComposite = WidgetFactory.composite(tabFolder, new GridLayout(4, false));
		interactionComposite = WidgetFactory.composite(tabFolder, new GridLayout(4, false));
		constrainteComposite = WidgetFactory.composite(tabFolder, new GridLayout(5, true));
		detailsComposite = WidgetFactory.composite(tabFolder, new GridLayout(2, false));
		logLabel = WidgetFactory.label(tabFolder, Logger.getLogs());

		// Tabs
		WidgetFactory.tabItem(tabFolder, "Features", WidgetFactory.scrolledComposite(tabFolder, featureComposite));
		WidgetFactory.tabItem(tabFolder, "Interactions", WidgetFactory.scrolledComposite(tabFolder, interactionComposite));
		WidgetFactory.tabItem(tabFolder, "Constraints", constrainteComposite);
		WidgetFactory.tabItem(tabFolder, "Details", WidgetFactory.scrolledComposite(tabFolder, detailsComposite));
		WidgetFactory.tabItem(tabFolder, "Logs", WidgetFactory.scrolledComposite(tabFolder, logLabel));

		// Default messages
		WidgetFactory.label(featureComposite, "Please select a configuration");
		WidgetFactory.label(interactionComposite, "Please select a configuration");
		
		// Constraint tab content 
		WidgetFactory.label(constrainteComposite, "Include", new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		WidgetFactory.label(constrainteComposite, "");
		WidgetFactory.label(constrainteComposite, "Available", new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		WidgetFactory.label(constrainteComposite, "");
		WidgetFactory.label(constrainteComposite, "Exclude", new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		
		whiteList = WidgetFactory.list(constrainteComposite);
		WidgetFactory.button(constrainteComposite, new GridData(SWT.CENTER, SWT.BOTTOM, false, false, 1, 1), "Add", e -> updateConstraints(freeList, configImprovement::addInclusion));
		freeList = WidgetFactory.list(constrainteComposite);
		WidgetFactory.button(constrainteComposite, new GridData(SWT.CENTER, SWT.BOTTOM, false, false, 1, 1), "Add", e -> updateConstraints(freeList, configImprovement::addExclusion));
		blackList = WidgetFactory.list(constrainteComposite);
		WidgetFactory.button(constrainteComposite, new GridData(SWT.CENTER, SWT.TOP, false, false, 1, 1), "Remove", e -> updateConstraints(whiteList, configImprovement::removeInclusion));
		WidgetFactory.button(constrainteComposite, new GridData(SWT.CENTER, SWT.TOP, false, false, 1, 1), "Remove", e -> updateConstraints(blackList, configImprovement::removeExclusion));
		
		detectCurrentConfig();
	}
	
	@Override
	public void updateLogs() {
		this.logLabel.setText(Logger.getLogs());
		this.logLabel.redraw();
		((ScrolledComposite) this.logLabel.getParent()).setMinSize(logLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		this.logLabel.redraw();
	}
	
	public void applyAll(Optimization optimization, Composite parent) {
		Logger.log("Applying all " + optimization.toString() + " suggestions" );
		addSuggestionButton(optimization, parent);
		
		long startFeature = System.currentTimeMillis();
		java.util.List<Suggestion> applied = configImprovement.applyAllSuggestions(optimization);
		long endFeature = System.currentTimeMillis();
		
		boolean saved = configImprovement.save();
			
		applied.forEach(f -> Logger.log("Automatically applied " + f.toString()));
		
		String result = "Applied " + applied.size() + " suggestions in " + (endFeature - startFeature) + "ms, "+ (saved ? "successfully saved" : "failed to save");
		Logger.log(result);
		WidgetFactory.label(parent, result);
		parent.layout(true);
	}
	
	public void apply(Suggestion suggestion) {
		Logger.log("applying: " + suggestion.toString());
		configImprovement.applySuggestion(suggestion);
		boolean saved = configImprovement.save();
		Logger.log("Configuration " + (saved ? "successfully saved" : "failed to save"));
		ready();
	}
	
	public void updateConstraints(List originalList, Consumer<? super String> action) {
		Arrays.stream(originalList.getSelection()).forEach(action);
		ready();
	}
	
	public void detectCurrentConfig() {
		this.configImprovement = loadProject();
		if (configImprovement != null) {
			ready();
		} else {
			Logger.log("Could not load configuration");
		}
	}

	/**
	 * Locate the Feature Model and performance file of the projects, and the
	 * selected configuration
	 */
	public static ConfigImprovement loadProject() {
		IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		IEditorPart activeEditor = activePage.getActiveEditor();
		File workspaceDirectory = ResourcesPlugin.getWorkspace().getRoot().getLocation().toFile();

		IFile file = null;
		
		if (activeEditor != null) {
			ISelection selection = activePage.getSelection();
			if (selection instanceof IStructuredSelection) {
				IStructuredSelection ssel = (IStructuredSelection) selection;
				Object obj = ssel.getFirstElement();
				if(obj != null) {
					file = (IFile) Platform.getAdapterManager().getAdapter(obj, IFile.class);
				}
				if (file == null && obj instanceof IAdaptable) {
					file = (IFile) ((IAdaptable) obj).getAdapter(IFile.class);
				}
			}
		}
		
		ConfigImprovement configImprovement = null;
		if (file != null) {
			Path configPath = Path.of(workspaceDirectory + file.getFullPath().toString());
			Path projectLocation = Path.of(file.getProject().getLocation().toString());
			try {
				configImprovement = FileHandling.loadProjectFiles(projectLocation, configPath);
			} catch (Exception e) {
				Logger.err(e.getMessage());
			}
		} else {
			Logger.log("No file selected");
		}

		return configImprovement;
	}
	
	public void ready() {
		addSuggestionButton(configImprovement.getFeatureOptimization(), this.featureComposite);
		addSuggestionButton(configImprovement.getInteractionOptimization(), this.interactionComposite);
		initConstraintsLists();
		fillDetails();
		
		Logger.log("Is configuration valid: " + configImprovement.isConfigValid());
		Logger.log("Can configuration be valid: " + configImprovement.canConfigValid());
		Logger.log("whitelist : " + configImprovement.getIncludeNames());
		Logger.log("blacklist : " + configImprovement.getExcludeNames());
		
		featureComposite.layout(true);
		interactionComposite.layout(true);
	}
	
	public void fillDetails() {
		Arrays.asList(detailsComposite.getChildren()).forEach(Control::dispose);
		
		Table featureTable = WidgetFactory.table(detailsComposite, "Features", "Performances", new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		if(!configImprovement.getFeatureOptimization().getPerformance().isEmpty()) {
			configImprovement.getPerformanceItems(configImprovement.getFeatureOptimization()).stream()
				.sorted(Comparator.comparing(ConfigPerformanceItem::getPerformance).reversed())
				.forEach(f -> WidgetFactory.tableItem(featureTable, f.getLabel(), f.getPerformance().intValue()));	
		}
		
		Table interactionTable = WidgetFactory.table(detailsComposite, "Interactions", "Performances", new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		if(!configImprovement.getFeatureOptimization().getPerformance().isEmpty()) {
			configImprovement.getPerformanceItems(configImprovement.getInteractionOptimization()).stream()
				.sorted(Comparator.comparing(ConfigPerformanceItem::getPerformance).reversed())
				.forEach(f -> WidgetFactory.tableItem(interactionTable, f.getLabel(), f.getPerformance().intValue()));
		}
		
		((ScrolledComposite) detailsComposite.getParent()).setMinSize(detailsComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		detailsComposite.layout(true);
	}
	
	public void addSuggestionButton(Optimization optimization, Composite parent) {
		Arrays.asList(parent.getChildren()).forEach(Control::dispose);
		
		Composite header = WidgetFactory.composite(parent, WidgetFactory.rowLayout(SWT.HORIZONTAL), SWT.NONE);
		header.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));
		
		String validity = configImprovement.isConfigValid() ? "configuration valide" : configImprovement.canConfigValid() ? "can be valide, completion mode" : "can't be valid";
		WidgetFactory.label(header, "Working on " + configImprovement.getPath().getFileName().toString() +  " - " + validity);
		if (optimization.getPerformance() != null) {
			WidgetFactory.label(header, "");
			WidgetFactory.button(header, null, "Find " + optimization.toString() + " suggestions", e -> fetchSuggestions(optimization, parent));
			WidgetFactory.button(header, null, "Apply all suggestions", e -> applyAll(optimization, parent));
		} else {
			WidgetFactory.label(header, "No Performance file found!");
		}
		
		parent.layout(true);
	}
	
	public void fetchSuggestions(Optimization optimization, Composite parent) {
		addSuggestionButton(optimization, parent);
	
		java.util.List<Suggestion> suggestions = configImprovement.getSuggestions(optimization, true);
		if(suggestions.isEmpty()) {
			WidgetFactory.label(parent, "No Suggestion");
		} 
		
		suggestions.forEach(s -> WidgetFactory.createSuggestionLabels(s, parent, this));
		
		parent.layout(true);
		((ScrolledComposite) parent.getParent()).setMinSize(parent.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}
	
	public void initConstraintsLists() {
		whiteList.removeAll();
		blackList.removeAll();
		freeList.removeAll();
		
		configImprovement.getIncludeNames().forEach(whiteList::add);
		configImprovement.getExcludeNames().forEach(blackList::add);
		configImprovement.getAllFeatureNames().stream()
				.filter(f -> !configImprovement.getExcludeNames().contains(f) && !configImprovement.getIncludeNames().contains(f))
				.forEach(freeList::add);
	}

	/**
	 * This method is kept for E3 compatiblity. You can remove it if you do not mix
	 * E3 and E4 code. <br/>
	 * With E4 code you will set directly the selection in ESelectionService and you
	 * do not receive a ISelection
	 * 
	 * @param s the selection received from JFace (E3 mode)
	 */
	@Inject
	@Optional
	public void setSelection(@Named(IServiceConstants.ACTIVE_SELECTION) ISelection s) {
		if (s == null || s.isEmpty())
			return;

		if (s instanceof IStructuredSelection) {
			IStructuredSelection iss = (IStructuredSelection) s;
			if (iss.size() == 1)
				setSelection(iss.getFirstElement());
			else
				setSelection(iss.toArray());
		}
	}

	/**
	 * This method manages the selection of your current object. In this example we
	 * listen to a single Object (even the ISelection already captured in E3 mode).
	 * <br/>
	 * You should change the parameter type of your received Object to manage your
	 * specific selection
	 * 
	 * @param o : the current object received
	 */
	@Inject
	@Optional
	public void setSelection(@Named(IServiceConstants.ACTIVE_SELECTION) Object o) {

		// Remove the 2 following lines in pure E4 mode, keep them in mixed mode
		if (o instanceof ISelection) // Already captured
			return;

		// Test if label exists (inject methods are called before PostConstruct)
		if (logLabel != null)
			detectCurrentConfig();
	}

	/**
	 * This method manages the multiple selection of your current objects. <br/>
	 * You should change the parameter type of your array of Objects to manage your
	 * specific selection
	 * 
	 * @param o : the current array of objects received in case of multiple
	 *          selection
	 */
	@Inject
	@Optional
	public void setSelection(@Named(IServiceConstants.ACTIVE_SELECTION) Object[] selectedObjects) {

		// Test if label exists (inject methods are called before PostConstruct)
		if (logLabel != null)
			detectCurrentConfig();
	}

}
