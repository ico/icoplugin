package fr.ico.plugin.parts;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ico.lib.optimization.storage.Suggestion;

public class SuggestionTab extends Composite {
	
	private List<Suggestion> suggestions;
	

	public SuggestionTab(Composite parent, int style, List<Suggestion> suggestions) {
		super(parent, style);
		this.suggestions = suggestions;
		initTab();
	}
	
	public void initTab() {
		if(suggestions.isEmpty()) {
			Label empty = new Label(this, SWT.PUSH);
			empty.setText("No Suggestion");
		}
		for(Suggestion suggestion : suggestions) {
			Label wbList = new Label(this, SWT.PUSH);
			wbList.setText("sugg : " + suggestion.toString());
		}
	}

}
