# ICOplugin


This project is the Eclipse plugin of the ICO tool suite.



## Developement
This project relies on the library [icolib.jar](https://gitlab.inria.fr/ico/icolib)

Complete installation on Eclipse:
- clone the project with ```git@gitlab.inria.fr:ico/icolib.git```
- Open the project with Eclipse: File -> "Open Project from file system" -> with the "directory" button, browse to the location where you cloned the project, and select the icoplugin folder. Select "open" to select this folder. Make sure that the "icoplugin" project is selected in the project import window and select "finish".
- Add icolib.jar to a specific location of the project: right-click on the icoplugin project in Eclipse, "new" -> "folder", in the folder create window, select "icoplugin" and set the folder name to "lib". Select "Finish". Copy icolib.jar in the lib folder.
- Add the dependancies to your classpath: Right click on the icolib project in Eclipse, "Build Path" -> "Configure Build Path". In the project properties window, select "classpath" and then "Add JARs". Select icoplugin/lib/icolib.jar and select "open". In the project properties window, select "Apply and close".
- Install the Eclipse PDE: in Eclipse, "Help" -> "Eclipse Marketplace" -> Search for "Eclipse PDE" -> "Install" and follow the installer's insctructions.
- Install FeatureIDE: in Eclipse, "Help" -> "Eclipse Marketplace" -> Search for "FeatureIDE" -> "Install" -> select "Confirm" on the suggested configuration and follow the installer's insctructions.

To launch the project: 
- Right-click on the icoplugin project in Eclipse -> "Run as" -> "Run configuration"
- In the "Run configuration" window, create an "Eclipse Application" configuration. Select the "Configuration" tab and make sure that "Support software installation in the launched application" is selected. Select "Apply" and then "Run".


You can know create new projects and install plugins in this Eclispe Runtime. 

## Usage
Open the ICOP view: "Window" -> "Show view" -> "Other" -> Select "ICO" > "ICOplugin" -> "Open"

ICOP detects the currently selected project and configuration. Select a configuration in Eclipse' project explorer.

ICOP does not support invalide configuration (i.e. it support valid and partial configuration).

### Suggestions
The "Features" and "Interactions" tabs display the name of the current configuration and its status.

Upon detection of a supported configuration and the corresponding performance file, the "Features" and "Interactions" tabs will display a button to list suggestions, and a button to apply all best suggestions.

Each suggestion can be applied with their respective "Apply" button.


### Constraints
The Constraints tab allow the management of constraints.

Available features can be added or remove from the Include or Exclude lists.

Upon modification of constraints, the suggestions are reset.

### Details
The Details tab display the performance of each feature and each pair of feature currently in the configuration

### Logs 
The Logs tab displays the logs of ICOplugin and ICOlib for technical use.


## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
If you want to contribute to this project please create a merge request.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.


